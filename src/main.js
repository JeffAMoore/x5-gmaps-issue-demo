import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import MapOne from "./components/MapOne";
import MapTwo from "./components/MapTwo";
import x5GMaps from 'x5-gmaps'

Vue.config.productionTip = false
Vue.use(VueRouter)


const googleMapsAPIkey = 'AIzaSyAeaxcBxWhKmsHxhpmibAOOFu2NyG0iSdg'
// Dev key: AIzaSyAeaxcBxWhKmsHxhpmibAOOFu2NyG0iSdg
Vue.use(x5GMaps, googleMapsAPIkey)

const routes = [
  { path: '/', component: MapOne },
  { path: '/map-two', component: MapTwo }
]

const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
